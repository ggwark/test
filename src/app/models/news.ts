export interface News {
  id: number;
  title: string;
  description: string;
  author: string;
  date: Date;
}


// {id: 1, title: 'Lorem text lorem', description: 'some long text', author: 'Tom Cuk', date: new Date()},
