import { Injectable } from '@angular/core';
import { News } from 'src/app/models/news';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }
  // private listNews: News[] = [
  //   {id: 1, title: 'Lorem text lorem', description: 'some long text', author: 'Tom Cuk', date: new Date()},
  //   {id: 2, title: 'Lorem texttext', description: 'some lonlonglonglongg text', author: 'Tom Cuk1', date: new Date()},
  //   {id: 3, title: 'LoremLorem text', description: 'some long longlonglonglong', author: 'Tom Cuk2', date: new Date()},
  //   {id: 4, title: 'Lorem LoremLorem', description: 'somtexttexttexttexte long text', author: 'Tom Cuk3', date: new Date()},
  //   {id: 5, title: 'texttext text', description: 'some ltexttexttextong text', author: 'Tom Cuk4', date: new Date()},
  //   {id: 6, title: 'Lorem textLorem', description: 'some long text', author: 'Tom Cuk5', date: new Date()},
  //   {id: 7, title: 'Lorem text', description: 'somtexttexttexte long text', author: 'Tom Cuk6', date: new Date()},
  //   {id: 8, title: 'texttexttext text', description: 'solonglonglongme long text', author: 'Tom Cuk7', date: new Date()},
  //   {id: 9, title: 'Lortexttexttexttextem text', description: 'some long text', author: 'Tom Cuk9', date: new Date()},
  //   {id: 10, title: 'Loremtexttexttexttext text', description: 'somlonglonglonge long text', author: 'Tom Cuk8', date: new Date()},
  //   {id: 11, title: 'Lorem tLoremLoremLoremext', description: 'some long texttexttexttext', author: 'Tom Cuk12', date: new Date()},
  //   {id: 12, title: 'Lorem tLoremext', description: 'sotexttexttextme long text', author: 'Tom Cuk22', date: new Date()},
  //   {id: 13, title: 'Lorem tLoremLoremext', description: 'some longlonglonglonglong text', author: 'Tom Cuk33', date: new Date()},
  // ];

  private selected: News;

  getSelectedNews(): News {
    return this.selected;
  }

  setSelectedNews(news: News) {
    this.selected = news;
  }

  // getNews(): News[] {
  //   return this.listNews;
  // }

  // getNewsById(id: number): News {
  //   this.selected = this.listNews.find(item => item.id === id);
  //   return this.selected;
  // }


  getData(): Observable<News[]> {
    return this.http.get<News[]>('assets/news.json');
  }

  getDataById(id): Observable<News> {
    return this.http.get<News>('assets/news.json').pipe(map((res: any) => {
       return res.find(item => item.id === id);
    }));
  }

}
