import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsListComponent } from './news-list/news-list.component';
import { NewCardComponent } from './new-card/new-card.component';
import { ItemNewsComponent } from './item-news/item-news.component';


@NgModule({
  declarations: [NewsListComponent, NewCardComponent, ItemNewsComponent],
  imports: [
    CommonModule,
    NewsRoutingModule
  ]
})
export class NewsModule { }
