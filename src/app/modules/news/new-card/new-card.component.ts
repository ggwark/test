import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../service/news.service';
import { News } from 'src/app/models/news';

@Component({
  selector: 'app-new-card',
  templateUrl: './new-card.component.html',
  styleUrls: ['./new-card.component.scss']
})
export class NewCardComponent implements OnInit {

  constructor(private route: ActivatedRoute, private newsService: NewsService) {
    route.params.subscribe(params => {
      this.currentId = +params.id;
      this.getData();
    });
   }
   currentId: number;
   currentItem: News;

  ngOnInit(): void {}

  getData() {

    if (this.newsService.getSelectedNews() && (this.newsService.getSelectedNews().id === this.currentId)) {
      this.currentItem = this.newsService.getSelectedNews();
    } else {
      this.getItemById(this.currentId);
      // this.currentItem = this.newsService.getNewsById(this.currentId);
    }


    // console.log(this.currentItem);
  }

  getItemById(id) {
    this.newsService.getDataById(id).subscribe(res => {
      this.currentItem = res;
      console.log(res);
    });
  }



}
